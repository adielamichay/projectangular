import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
@Component({
  selector: 'affenpinschers',
  templateUrl: './affenpinschers.component.html',
  styleUrls: ['./affenpinschers.component.css']
})
export class AffenpinschersComponent implements OnInit {
  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  

  ngOnInit() {
  }

}
