import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import {TabModule} from 'angular-tabs-component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';

import { Routes,RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AlskenMelmoteComponent } from './alsken-melmote/alsken-melmote.component';
import { AffenpinschersComponent} from './affenpinschers/affenpinschers.component';
import { AfghanComponent }from './afghan/afghan.component';
import { AlskenKleeComponent } from './alsken-klee/alsken-klee.component';
import { AmericanBulldogComponent} from './american-bulldog/american-bulldog.component';
import { AmericanEskimosComponent} from './american-eskimos/american-eskimos.component';
import { AmericanPitComponent} from './american-pit/american-pit.component';
import { AmstaffComponent} from './amstaff/amstaff.component';
import {JapanisAkitaComponent} from './japanis-akita/japanis-akita.component';
import { from } from 'rxjs';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    AlskenMelmoteComponent,
    AffenpinschersComponent,
    AfghanComponent,
    AlskenKleeComponent,
    AmericanBulldogComponent,
    AmericanEskimosComponent,
    AmericanPitComponent,
    AmstaffComponent,
    JapanisAkitaComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatTooltipModule,
    TabModule,
    MatIconModule,
    RouterModule.forRoot([
      {path:'',component:WelcomeComponent},
      {path:'alskenMelmote',component:AlskenMelmoteComponent},
      {path:'affenpinschers',component:AffenpinschersComponent},
      {path:'afghan',component:AfghanComponent}, 
      {path:'alskenKlee',component:AlskenKleeComponent},
      {path:'americanBulldog',component:AmericanBulldogComponent},
      {path:'americanEskimos',component:AmericanEskimosComponent},
      {path:'americanPit',component:AmericanPitComponent}, 
      {path:'amstaff',component:AmstaffComponent},
      {path:'japanisAkita',component:JapanisAkitaComponent},
      {path:'alskenKlee',component:AlskenKleeComponent},
      {path:'**',component:WelcomeComponent}
      
      
   ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
