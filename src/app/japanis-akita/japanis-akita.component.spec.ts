import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JapanisAkitaComponent } from './japanis-akita.component';

describe('JapanisAkitaComponent', () => {
  let component: JapanisAkitaComponent;
  let fixture: ComponentFixture<JapanisAkitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JapanisAkitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JapanisAkitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
