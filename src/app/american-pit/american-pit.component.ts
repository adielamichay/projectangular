import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-american-pit',
  templateUrl: './american-pit.component.html',
  styleUrls: ['./american-pit.component.css']
})
export class AmericanPitComponent implements OnInit {

  welcome(){
    this.router.navigate(['/welcome']);
  }
  constructor(private router:Router) { }
  
  ngOnInit() {
  }

}
