import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmericanPitComponent } from './american-pit.component';

describe('AmericanPitComponent', () => {
  let component: AmericanPitComponent;
  let fixture: ComponentFixture<AmericanPitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmericanPitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmericanPitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
