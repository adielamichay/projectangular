import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmericanBulldogComponent } from './american-bulldog.component';

describe('AmericanBulldogComponent', () => {
  let component: AmericanBulldogComponent;
  let fixture: ComponentFixture<AmericanBulldogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmericanBulldogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmericanBulldogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
